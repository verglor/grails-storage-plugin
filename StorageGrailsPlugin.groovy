import grails.plugin.storage.*

class StorageGrailsPlugin {

    def version = "0.4-SNAPSHOT"
    def grailsVersion = "2.1 > *"
    def dependsOn = [:]
    def pluginExcludes = [
        "grails-app/views/error.gsp"
    ]

    def title = "Storage Plugin" // Headline display name of the plugin
    def author = "Jaroslav Kostal"
    def authorEmail = "jaroslav@kostal.sk"
    def description = '''\
'''

    def documentation = "http://grails.org/plugin/storage"

    def license = "APACHE"

//    def organization = [ name: "My Company", url: "http://www.my-company.com/" ]

//    def developers = [ [ name: "Joe Bloggs", email: "joe@bloggs.net" ]]

//    def issueManagement = [ system: "JIRA", url: "http://jira.grails.org/browse/GPSTORAGE" ]

    // Online location of the plugin's browseable source code.
//    def scm = [ url: "http://svn.codehaus.org/grails-plugins/" ]

    def doWithWebDescriptor = { xml ->
        // TODO Implement additions to web.xml (optional), this event occurs before
    }

    def doWithSpring = {

		def storageConfig = application.config.grails?.plugin?.storage

		fileStorage(FileStorage) { bean ->
			dir = storageConfig?.file?.dir ?: ''
		}
		
		s3Storage(S3Storage) { bean ->
			bean.autowire = 'byName'
			bucketName = storageConfig?.s3?.bucket?.name ?: ''
			prefix = storageConfig?.s3?.prefix ?: ''
		}
		
    }

    def doWithDynamicMethods = { ctx ->
        // TODO Implement registering dynamic methods to classes (optional)
    }

    def doWithApplicationContext = { ctx ->
		def storageType = ctx.grailsApplication.config.grails?.plugin?.storage?.type ?: 'file'
		ctx.storageService.storage = ctx.getBean(storageType + 'Storage')
    }

    def onChange = { event ->
        // TODO Implement code that is executed when any artefact that this plugin is
        // watching is modified and reloaded. The event contains: event.source,
        // event.application, event.manager, event.ctx, and event.plugin.
    }

    def onConfigChange = { event ->
        // TODO Implement code that is executed when the project configuration changes.
        // The event is the same as for 'onChange'.
    }

    def onShutdown = { event ->
        // TODO Implement code that is executed when the application shuts down (optional)
    }
}
