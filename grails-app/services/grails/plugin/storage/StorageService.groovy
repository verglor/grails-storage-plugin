package grails.plugin.storage

import org.codehaus.groovy.grails.plugins.codecs.SHA256BytesCodec

class StorageService {

	static transactional = false
	
	Storage storage
	
	String computeHash(byte[] content) {
		Base62Codec.encode(SHA256BytesCodec.encode(content))
	}
	
	String store(byte[] content, String contentType = null, String filename = null) {

		def key = computeHash(content)
		storage.store(key, content, contentType, filename)
		key

	}
	
	byte[] retrieve(String key) {
		storage.retrieve(key)
	}
	
	void delete(String key) {
		storage.delete(key)
	}

}
