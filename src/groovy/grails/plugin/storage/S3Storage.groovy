package grails.plugin.storage

import com.amazonaws.services.s3.model.*
import java.net.URLEncoder

class S3Storage implements Storage {

	def amazonWebService
	def bucketName
	def prefix = ''

	private getMetadata(key) {
		
		try {
			amazonWebService.s3.getObjectMetadata(bucketName, prefix + key)
		} catch (AmazonS3Exception e) {
			if (e.statusCode == 404) return null
			throw e
		}
		
	}
		
	void store(String key, byte[] content, String contentType = null, String filename = null) {

		def existingMetadata = getMetadata(key)
		def metadata = existingMetadata ?: new ObjectMetadata(contentLength: content.size())
		metadata.cacheControl = 'public, s-maxage=31536000, max-age=31536000'
		if (contentType) metadata.contentType = contentType
		if (filename) metadata.contentDisposition = 'inline; filename="' + URLEncoder.encode(filename, 'UTF-8') + '"'

		if (existingMetadata) {

			if (existingMetadata.cacheControl == metadata.cacheControl &&
			    existingMetadata.contentType == metadata.contentType &&
			    existingMetadata.contentDisposition == metadata.contentDisposition) return

			log.info "Updating existing S3 object [${prefix + key}]..."
			amazonWebService.s3.copyObject(
				new CopyObjectRequest(bucketName, prefix + key, bucketName, prefix + key)
					.withCannedAccessControlList(CannedAccessControlList.PublicRead)
					.withNewObjectMetadata(metadata)
			)
		} else {
			log.info "Uploading new S3 object [${prefix + key}]..."
			amazonWebService.s3.putObject(
				new PutObjectRequest(bucketName, prefix + key, new ByteArrayInputStream(content), metadata)
					.withCannedAcl(CannedAccessControlList.PublicRead)
			)
		}
		
	}
	
	byte[] retrieve(String key){
		
		try {
			def object = amazonWebService.s3.getObject(bucketName, prefix + key)
			def bytes = object.objectContent.bytes
			object.close()
			return bytes
		} catch (AmazonS3Exception e) {
			if (e.statusCode == 404) return null
			throw e
		}
		
	}
	
	public void delete(String key) {
		log.info "Deleting S3 object [${prefix + key}]..."
		amazonWebService.s3.deleteObject(bucketName, prefix + key)
	}

}
