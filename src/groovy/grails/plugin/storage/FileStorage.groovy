package grails.plugin.storage

//import org.apache.commons.io.FileUtils;

class FileStorage implements Storage {

	def dir = '' 
	
	def setDir(dir) {
		// add trailing slash to non-empty directory if missing 
		this.dir = dir ? dir + (dir.endsWith('/') ? '' : '/') : ''
	}
	
	void store(String key, byte[] content, String contentType = null, String filename = null) {
	
		new File(dir).mkdirs()
		new File(dir + key).bytes = content
//		FileUtils.forceMkdir(new File(dir))	
//		FileUtils.writeByteArrayToFile(new File(dir + key), content, false)
		
	}
	
	byte[] retrieve(String key){
		
		try {
			new File(dir + key).bytes
		} catch (FileNotFoundException) {
			return null
		}
		
	}

	public void delete(String key) {
		File file = new File(dir + key)
		if (file.exists()) file.delete()
	}
	
}
