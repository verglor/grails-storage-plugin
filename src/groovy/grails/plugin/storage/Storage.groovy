package grails.plugin.storage

public interface Storage {

	/**
	 * Stores object with specified content and filename under provided key in storage.
	 * 
	 * @param key
	 * @param content
	 * @param contentType
	 * @param filename
	 */
	void store(String key, byte[] content, String contentType, String filename)
	
	/**
	 * Retrieves object from storage with specified key.
	 * 
	 * @param key
	 * @return byte array of content or null if key does not exist.
	 */
	byte[] retrieve(String key);

	/**
	 * Deletes object from storage with specified key.
	 * 
	 * @param key
	 */
	void delete(String key);

}
